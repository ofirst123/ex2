import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'listitem',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {
  @Input() data:any;
  @Input() data2:any;

  title;
  id;
  price;
  stock;
  color = 'green';


  constructor() { }

  ngOnInit() {
    this.title = this.data.title;
    this.id = this.data.id;
    this.price = this.data.price;
    this.stock = this.data.stock;

    if (this.stock < 10) {
      this.color = 'red';
    }
  }

}
